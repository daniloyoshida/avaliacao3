package utfpr.ct.dainf.pratica;

import java.util.ArrayList;
import java.util.List;

/**
 * UTFPR - Universidade Tecnológica Federal do Paraná
 * DAINF - Departamento Acadêmico de Informática
 * 
 * @author Wilson Horstmeyer Bogado <wilson@utfpr.edu.br>
 * @param <T> Tipo do ponto
 */
public class Poligonal<T extends Ponto2D> {
    
    final List<T> vertices = new ArrayList<>();
    
    int n;
    private T i;

    public Poligonal(int n) {
        this.n = n;
    }
    
    public int getN(){
        return n;
    }
        
    public Ponto getVertice(T i){
        return i;
        
        if(i>=0 && i <= vertices.lenght){
            return vertices[i];
        }else{
            throw new RuntimeException("get(" + i + "): índice inválido");
        }
        
    public setVertice(int i, T p){
            if(i >= 0 && i <= vertices.lenght){
                double x = p.getX();
                double y = p.getY();
            }
                      
        }
    public double getcomprimento() {
        double com=0;
        for(int j=1; j < this.vertices.length; j++) {
            com += Math.sqrt( Math.pow((this.vertices.get(j).getX() 
	       - this.vertices.get(j-1).getX() ), 2)
              + Math.pow(( this.vertices.get(j).getY() 
	       - this.vertices.get(j-1).getY() ), 2) ); }
        return com; 
    
    }

    @Override
    public String toString() {
        return "Poligonal{" + "vertices=" + vertices + ", n=" + n + ", i=" + i + '}';
    }
}
